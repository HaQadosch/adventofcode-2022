const _sampleFile = "sample.txt";
const _inputFile = "Day01_input.txt";

const contentFile = await Deno.readTextFile(_inputFile);
/**
 * Example of string is "1000\n2000\n3000\n\n4000\n\n5000\n6000\n\n7000\n8000\n9000\n\n10000"
 * Elves are separated by 2 returns "\n\n"
 * and CalorieItems are separated with 1 return "\n"
 */
const _calorieItems = contentFile
  .split("\n\n")
  .map((elf) => elf.split("\n"));

/**
 * calorieItems:
  [
    [ "13860", "5324", "10272", "10859", "1615", "1345" ],
    [ "21147", "20301", "24255" ],
    [ "57301" ],
    [ "16407", "9592", "3822", "11186", "9812" ],
    [ "2436", "9609", "5163", "6263", "4182", "10301" ],
    [ "22997", "3077", "15477" ]
  ]
 */

export const totalCalorieItems = (items: string[]) =>
  items.reduce(
    (totalCalories, currentItem) =>
      totalCalories + Number.parseInt(currentItem, 10),
    0,
  );

export const totalPerElf = (calorieItems: string[][]) =>
  calorieItems.map((elf, index) => [index + 1, totalCalorieItems(elf)]);

/**
 * totalPerElf:
  [
    [ 1, 55445 ],  [ 2, 54093 ],  [ 3, 57505 ],  [ 4, 21479 ],
    [ 5, 43973 ],  [ 6, 53826 ],  [ 7, 50741 ],  [ 8, 48926 ]
  ]
 */

export const orderedTotal = (elves: number[][]) =>
  elves.sort((
    [_elfA, caloriesA],
    [_elfB, caloriesB],
  ) => caloriesB - caloriesA); // We want it in decreasing order, starting with the highest load.

// console.log({ orderedTotal: orderedTotal(totalPerElf(calorieItems)) });

// Run with: deno run --allow-read Day01.ts
// right answer: 67016
console.log({ top3: 67016 + 66601 + 66499 });
