const _sampleFile = "Day02_sample.txt";
const _inputFile = "Day02_input.txt";

const contentFile = await Deno.readTextFile(_inputFile);
/**
 * { contentFile: "A Y\nB X\nC Z" }
 * A for Rock, B for Paper, and C for Scissors
 * X for Rock, Y for Paper, and Z for Scissors
 * Rock defeats Scissors, Scissors defeats Paper, and Paper defeats Rock
 * A > Z
 * C > Y
 * B > X
 *
 * score for the shape you selected:
 *  1 for Rock, 2 for Paper, and 3 for Scissors
 * score for the outcome of the round:
 *  0 if you lost, 3 if the round was a draw, and 6 if you won
 */
const scoring: { [key: string]: number } = {
  "A X": 1 + 3,
  "A Y": 2 + 6,
  "A Z": 3 + 0,
  "B X": 1 + 0,
  "B Y": 2 + 3,
  "B Z": 3 + 6,
  "C X": 1 + 6,
  "C Y": 2 + 0,
  "C Z": 3 + 3,
};

const matches = contentFile.split("\n");

export const scores = (matches: string[]) =>
  matches.map((match) => scoring[match]);

export const totalScore = (scores: number[]) =>
  scores.reduce((acc, current) => acc + current, 0);

const myTotal = totalScore(scores(matches));
console.log({ scores: scores(matches), myTotal }); // 11386

/**
 * { contentFile: "A Y\nB X\nC Z" }
 * A for Rock, B for Paper, and C for Scissors
 * X you lose, Y you end in a draw, and Z you win
 * Rock defeats Scissors, Scissors defeats Paper, and Paper defeats Rock
 * A > C
 * C > B
 * B > A
 *
 * score for the shape you selected:
 *  1 for Rock, 2 for Paper, and 3 for Scissors
 * score for the outcome of the round:
 *  0 if you lost, 3 if the round was a draw, and 6 if you won
 */
const scoring2: { [key: string]: number } = {
  "A X": 3 + 0,
  "A Y": 1 + 3,
  "A Z": 2 + 6,
  "B X": 1 + 0,
  "B Y": 2 + 3,
  "B Z": 3 + 6,
  "C X": 2 + 0,
  "C Y": 3 + 3,
  "C Z": 1 + 6,
};

export const scores2 = (matches: string[]) =>
  matches.map((match) => scoring2[match]);

export const totalScore2 = (scores: number[]) =>
  scores.reduce((acc, current) => acc + current, 0);

const myTotal2 = totalScore2(scores2(matches));
console.log({ scores2: scores2(matches), myTotal2 }); // 13600
