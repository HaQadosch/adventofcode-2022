import { assertEquals } from "https://deno.land/std@0.167.0/testing/asserts.ts";
import { orderedTotal, totalCalorieItems, totalPerElf } from "./Day01.ts";

Deno.test("Sum calories for some items", () => {
  const calorieItems = ["2436", "9609", "5163", "6263", "4182", "10301"];
  const expectedTotalCalories = 2436 + 9609 + 5163 + 6263 + 4182 + 10301;

  assertEquals(totalCalorieItems(calorieItems), expectedTotalCalories);
});

Deno.test("Sum calories for no items", () => {
  const calorieItems: string[] = [];
  const expectedTotalCalories = 0;

  assertEquals(totalCalorieItems(calorieItems), expectedTotalCalories);
});

Deno.test("Sum calories for one items", () => {
  const calorieItems = ["300"];
  const expectedTotalCalories = 300;

  assertEquals(totalCalorieItems(calorieItems), expectedTotalCalories);
});

Deno.test("List total calories for some elves", () => {
  const listCalories = [
    ["13860", "5324", "10272", "10859", "1615", "1345"], // 43275
    ["21147", "20301", "24255"], // 65703
    ["57301"], // 57301
  ];
  const expectedListOfTotalCalories = [
    [1, 43275],
    [2, 65703],
    [3, 57301],
  ];

  assertEquals(totalPerElf(listCalories), expectedListOfTotalCalories);
});

Deno.test("List total calories for no elves", () => {
  const listCalories: string[][] = [];
  const expectedListOfTotalCalories: number[][] = [];

  assertEquals(totalPerElf(listCalories), expectedListOfTotalCalories);
});

Deno.test("Order the list per the amount of calories", () => {
  const listElves = [
    [1, 55445],
    [2, 54093],
    [3, 57505],
    [4, 21479],
  ];
  const expectedOrderedListElves = [
    [3, 57505],
    [1, 55445],
    [2, 54093],
    [4, 21479],
  ];

  assertEquals(orderedTotal(listElves), expectedOrderedListElves);
});

/**
 * Since we import from a file, we need to be allowed to read that file.
 * test with:
 *  deno test --allow-read
 */
