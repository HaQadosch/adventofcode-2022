const _sampleFile = "Day03_sample.txt";
const _inputFile = "Day03_input.txt";

const contentFile = await Deno.readTextFile(_sampleFile);
const rukSacks = contentFile.split("\n");

const compartementalise = (rukSack: string): [string, string] => {
  const middle = rukSack.length / 2;
  const compartment1 = rukSack.slice(0, middle);
  const compartment2 = rukSack.slice(middle);
  return [compartment1, compartment2];
};

const duplicates = ([comp1, comp2]: [string, string]): Set<string> => {
  return comp1
    .split("")
    .reduce((acc, currentItem) => {
      if (comp2.includes(currentItem)) {
        acc.add(currentItem);
      }
      return acc;
    }, new Set<string>());
};

const computePriorities = (itemType: string): number => {
  return (isLowerCase(itemType) ? prioLowerCase : prioUpperCase) + itemType.charCodeAt(0)
}

const prioLowerCase = 1 - "a".charCodeAt(0)
const prioUpperCase = 1 - "A".charCodeAt(0)

function isLowerCase (letter: string) {
  return letter === letter.toLowerCase()
}

const duplicatesAll = rukSacks.map((rukSack) =>
  duplicates(compartementalise(rukSack))
);

const prioItems = duplicatesAll.map(computePriorities)

/**
 * "A".charCodeAt(0) = 65
 * "Z" = 90
 * "a" = 97
 * "z" = 122
 * Lowercase item types a through z have priorities 1 through 26.
 * Uppercase item types A through Z have priorities 27 through 52.
 */
console.log({
  contentFile,
  rukSacks,
  dups: duplicatesAll,
  prios: prioItems
});
