import { assertEquals } from "https://deno.land/std@0.167.0/testing/asserts.ts";
import { scores, totalScore } from "./Day02.ts";

Deno.test("List scores for some matches", () => {
  const matches = ["A Y", "B X", "C Z"];
  const expectedScores = [8, 1, 6];

  assertEquals(scores(matches), expectedScores);
});

Deno.test("Sum the score of each match for a total", () => {
  const scores = [8, 1, 6];
  const expectedTotal = 15;

  assertEquals(totalScore(scores), expectedTotal);
});
