// Compute the priority of a given item type
function computePriority(itemType: string): number {
  // Check if the item type is lowercase
  if (itemType.toLowerCase() === itemType) {
    // If it is, its priority is the ASCII value of the character minus 96
    return itemType.charCodeAt(0) - 96;
  } // Otherwise, it must be uppercase
  else {
    // In this case, its priority is the ASCII value of the character minus 64
    return itemType.charCodeAt(0) - 64;
  }
}

export function totalPriority(rucksacks: string[]) {
  let totalPrio = 0;
  // Iterate over the rucksacks
  for (const rucksack of rucksacks) {
    // Keep track of the items that appear in both compartments
    const itemsInBothCompartments = [];

    // Compute the length of each compartment
    const compartmentLength = rucksack.length / 2;

    // Iterate over the items in the first compartment
    for (let i = 0; i < compartmentLength; i++) {
      // Check if the item also appears in the second compartment
      if (rucksack.slice(compartmentLength).includes(rucksack[i])) {
        // If it does, add it to the list of items that appear in both compartments
        itemsInBothCompartments.push(rucksack[i]);
      }
    }

    // Compute the sum of the priorities of the items that appear in both compartments
    totalPrio += itemsInBothCompartments.reduce(
      (total, item) => total + computePriority(item),
      0,
    );
  }
  return totalPrio;
}

// Print the total priority
console.log({ totalPriority });
